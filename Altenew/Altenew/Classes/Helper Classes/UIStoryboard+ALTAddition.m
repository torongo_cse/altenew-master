//
//  UIStoryboard+ALTAddition.m
//  Altenew
//
//  Created by Torongo Azad on 2017-01-21.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import "UIStoryboard+ALTAddition.h"

#define k_Altenew_Storyboard @"Altenew"
#define k_AltenewEditor_Storyboard @"AltenewEditor"

@implementation UIStoryboard (ALTAddition)

+ (UIStoryboard *)altenewStoryboard
{
    return [UIStoryboard storyboardWithName:k_Altenew_Storyboard bundle:[NSBundle mainBundle]];
}

+ (UIStoryboard *)altenewEditorStoryboard
{
    return [UIStoryboard storyboardWithName:k_AltenewEditor_Storyboard bundle:[NSBundle mainBundle]];
}

@end
