//
//  ALTHomeViewController.h
//  Altenew
//
//  Created by Torongo Azad on 2017-03-30.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALTHomeViewController : UIViewController

+ (instancetype)initializeHomeViewController;

@end
