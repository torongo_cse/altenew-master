//
//  ALTConstants.h
//  Altenew
//
//  Created by Torongo Azad on 2017-01-21.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIStoryboard+ALTAddition.h"

#define k_IS_SIGNED_IN @"com.altenew.signedIn"
#define k_SHOULD_SKIP_WELCOME_VIDEO @"com.altenew.skipWelcomVideo"


@interface ALTConstants : NSObject

@end
