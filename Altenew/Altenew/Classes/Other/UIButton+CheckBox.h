//
//  UIButton+CheckBox.h
//  Altenew
//
//  Created by Torongo Azad on 2017-01-24.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CheckBox)

@property (assign, nonatomic, getter=isChecked) BOOL checked;

- (void)updateCheckboxWithStatus:(BOOL)checked;

@end
