//
//  UIButton+CheckBox.m
//  Altenew
//
//  Created by Torongo Azad on 2017-01-24.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import "UIButton+CheckBox.h"
#import <objc/runtime.h>

static void *key = @"altenew.checkbox.unique.key";

@implementation UIButton (CheckBox)

- (void)setChecked:(BOOL)checked
{
    NSNumber *isCheckedWrapper = [NSNumber numberWithBool:checked];
    objc_setAssociatedObject(self, key, isCheckedWrapper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isChecked
{
    NSNumber *isCheckedWrapper = objc_getAssociatedObject(self, key);
    return [isCheckedWrapper boolValue];
}

- (void)updateCheckboxWithStatus:(BOOL)checked
{
    self.checked = checked;
    if (checked == YES) {
        [self setImage:[UIImage imageNamed:@"checked_checkbox"] forState:UIControlStateNormal];
    }else{
        [self setImage:[UIImage imageNamed:@"unchecked_checkbox"] forState:UIControlStateNormal];
    }
}

@end
