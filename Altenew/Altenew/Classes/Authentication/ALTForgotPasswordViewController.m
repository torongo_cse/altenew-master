//
//  ALTForgotPasswordViewController.m
//  Altenew
//
//  Created by Torongo Azad on 2017-01-21.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import "ALTForgotPasswordViewController.h"
#import "ALTConstants.h"

NSString *const ALTForgotPasswordViewControllerIdentifier = @"ALTForgotPasswordViewControllerID";

@interface ALTForgotPasswordViewController ()

@end

@implementation ALTForgotPasswordViewController

+ (instancetype)initializeForgotPasswordViewController
{
    return [[UIStoryboard altenewStoryboard] instantiateViewControllerWithIdentifier:ALTForgotPasswordViewControllerIdentifier];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
