//
//  ALTWelcomeVideoViewController.m
//  Altenew
//
//  Created by Torongo Azad on 2017-01-21.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import "ALTWelcomeVideoViewController.h"
#import "ALTLogInViewController.h"
#import "ALTConstants.h"
#import <MediaPlayer/MediaPlayer.h>

NSString *const ALTWelcomeVideoViewControllerIdentifier = @"ALTWelcomeVideoViewControllerID";


@interface ALTWelcomeVideoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *firstFrameImageView;
@property (strong, nonatomic) MPMoviePlayerViewController *playerViewController;

@end

@implementation ALTWelcomeVideoViewController

+ (instancetype)initializeWelcomeVideoViewController
{
    return [[UIStoryboard altenewStoryboard] instantiateViewControllerWithIdentifier:ALTWelcomeVideoViewControllerIdentifier];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    [self registerNotifications];
    NSURL *videoURL = [NSURL URLWithString:@"http://techslides.com/demos/sample-videos/small.mp4"] ;
    self.playerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    self.playerViewController.moviePlayer.shouldAutoplay = NO;
    [self.playerViewController.moviePlayer requestThumbnailImagesAtTimes:[NSArray arrayWithObject:[NSNumber numberWithDouble:1.0]] timeOption:MPMovieTimeOptionNearestKeyFrame];
}

- (void)registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishDownloadingThumbnail:) name:MPMoviePlayerThumbnailImageRequestDidFinishNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishPlayingVideo) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

- (void)didFinishDownloadingThumbnail:(NSNotification *)notification
{
    UIImage *thumbImage = [[notification userInfo] objectForKey:MPMoviePlayerThumbnailImageKey];
    self.firstFrameImageView.image = thumbImage;
}

- (void)didFinishPlayingVideo
{
    if (self.playerViewController != nil) {
        [self.playerViewController.moviePlayer stop];
    }
}

- (IBAction)skipButtonPressed:(id)sender {
    ALTLogInViewController *loginViewController = [ALTLogInViewController initializeLoginViewController];
    [self.navigationController pushViewController:loginViewController animated:YES];
}
- (IBAction)playButtonPressed:(id)sender {
    if (self.playerViewController != nil) {
        self.playerViewController = nil;
        NSURL *videoURL = [NSURL URLWithString:@"http://techslides.com/demos/sample-videos/small.mp4"] ;
        self.playerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
        self.playerViewController.moviePlayer.shouldAutoplay = YES;
        [self presentMoviePlayerViewControllerAnimated:self.playerViewController];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
