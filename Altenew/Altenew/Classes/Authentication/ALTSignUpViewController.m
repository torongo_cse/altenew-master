//
//  ALTSignUpViewController.m
//  Altenew
//
//  Created by Torongo Azad on 2017-01-21.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import "ALTSignUpViewController.h"
#import "ALTConstants.h"

#define MinimumHeight 400.0

NSString *const ALTSignUpViewControllerIdentifier = @"ALTSignUpViewControllerID";

@interface ALTSignUpViewController ()
{
    UITextField *activeTextField;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) UITapGestureRecognizer *tapGesture;

@end

@implementation ALTSignUpViewController

+ (instancetype)initializeSignUpViewController
{
    return [[UIStoryboard altenewStoryboard] instantiateViewControllerWithIdentifier:ALTSignUpViewControllerIdentifier];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.navigationController.navigationBarHidden = YES;

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateUIOnOrientationChange];
}

- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self updateUIOnOrientationChange];
}

- (void)updateUIOnOrientationChange
{
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
        CGRect contentViewFrame = self.contentView.frame;
        if (contentViewFrame.size.height < MinimumHeight) {
            contentViewFrame.size.height = MinimumHeight;
            self.scrollView.contentSize = contentViewFrame.size;
        }
    }
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    if (self.tapGesture == nil) {
        self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
        self.tapGesture.numberOfTapsRequired = 1;
        [self.view addGestureRecognizer:self.tapGesture];
    }
    NSDictionary *userInfo = notification.userInfo;
    CGRect keyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
    if (CGRectGetHeight(self.view.frame) - CGRectGetMaxY([self.view convertRect:activeTextField.frame fromView:[activeTextField superview]]) < CGRectGetHeight(keyboardFrame)) {
        CGFloat offset = CGRectGetHeight(keyboardFrame) - (CGRectGetHeight(self.view.frame) - CGRectGetMaxY([self.view convertRect:activeTextField.frame fromView:[activeTextField superview]]));
        if (offset > 0) {
            [self.scrollView setContentOffset:CGPointMake(0, offset + 20.0) animated:YES];
        }
    }
}
-(void)keyboardWillHide:(NSNotification *)notification
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    if (self.tapGesture != nil) {
        [self.view removeGestureRecognizer:self.tapGesture];
        self.tapGesture = nil;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
- (void)tappedOutside: (UIGestureRecognizer *)gesture
{
    [activeTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
