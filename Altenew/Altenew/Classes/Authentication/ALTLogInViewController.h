//
//  ALTLogInViewController.h
//  Altenew
//
//  Created by Torongo Azad on 2017-01-21.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALTLogInViewController : UIViewController

+ (instancetype)initializeLoginViewController;

@end
