//
//  AppDelegate.h
//  Altenew
//
//  Created by Torongo Azad on 2017-01-21.
//  Copyright © 2017 Altenew Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;

@end

